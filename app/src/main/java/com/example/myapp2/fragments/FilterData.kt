package com.example.myapp2.fragments

import android.content.Context
import com.example.myapp2.room.BarEntity
import com.example.myapp2.room.RestaurantEntity

class FilterData(
   var payEazyButtonState: Boolean
) {
    private var restaurants: List<RestaurantEntity> = listOf()
    private var bars: List<BarEntity> = listOf()

    fun setData(restaurants: List<RestaurantEntity>, bars: List<BarEntity>) {
        this.restaurants = restaurants
        this.bars = bars
    }

     fun getFilteredRestaurantData(): List<RestaurantEntity> {
        var filteredRestaurants = restaurants
        if (payEazyButtonState) {
            filteredRestaurants = filteredRestaurants.filter { it.payEazy == 1 }
        }
        return filteredRestaurants
    }

     fun getFilteredBarData(): List<BarEntity> {
        var filteredBars = bars
        if (payEazyButtonState) {
            filteredBars = filteredBars.filter { it.payEazy == 1 }
        }
        return filteredBars
    }
}
