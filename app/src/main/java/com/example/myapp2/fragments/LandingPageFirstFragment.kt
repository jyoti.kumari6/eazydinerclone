package com.example.myapp2.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapp2.ListItem1
import com.example.myapp2.ListItem3
import com.example.myapp2.R
import com.example.myapp2.activity.ListingRestaurantActivity
import com.example.myapp2.adapter.LandingPageAdapter
import com.example.myapp2.room.RestaurantDatabase
import com.example.myapp2.room.RestaurantEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LandingPageFirstFragment : Fragment(), OnCardClickListener {
    private lateinit var pickUpData: List<ListItem1>
    private lateinit var dineData: List<ListItem3>
    private lateinit var restaurantData: RestaurantDatabase
    private lateinit var restaurants: List<RestaurantEntity>
    private lateinit var upperHeading: List<String>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.landing_recycler_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        restaurantData = RestaurantDatabase.getRestaurantDataBase(requireContext())

        CoroutineScope(Dispatchers.IO).launch {
            restaurants = restaurantData.restaurantDao().getAll()
            pickUpData = listOf(
                ListItem1("Book a Table", "Up to 50% Off"),
                ListItem1("Prime", "25-50% Off"),
                ListItem1("Walk-In", "Up to 40% Off")
            )
            dineData = listOf(
                ListItem3("Dinner"),
                ListItem3("Lunch"),
                ListItem3("FastFood"),
                ListItem3("Near me")
            )
            upperHeading = listOf(
                "Add a banner here",
                "Hey Jyoti! Whats your pick?",
                "Recommended Restaurant",
                "Dine Anytime",
                "Dine Anytime"
            )

            withContext(Dispatchers.Main) {
                val recyclerView: RecyclerView = view.findViewById(R.id.recyclerView3)
                recyclerView.layoutManager = LinearLayoutManager(requireContext())
                recyclerView.adapter = LandingPageAdapter(
                    requireContext(),
                    pickUpData,
                    dineData,
                    restaurants,
                    upperHeading,
                    this@LandingPageFirstFragment
                )
            }
        }
    }

    override fun onCardClick() {
        val intent = Intent(requireContext(), ListingRestaurantActivity::class.java)
        startActivity(intent)
    }

}
