package com.example.myapp2.room


import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface RestaurantDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNewRestaurant(restaurantEntity: RestaurantEntity)

    @Delete
    suspend fun deleteRestaurantDetails(restaurantEntity: RestaurantEntity)

    @Update
    suspend fun updateRestaurantDetails(restaurantEntity: RestaurantEntity)

    @Query("SELECT * FROM RestaurantEntity LIMIT 10")
    suspend fun getAll(): List<RestaurantEntity>

    @Query("SELECT * FROM RestaurantEntity WHERE payEazy = 1")
    suspend fun getPayEazyAvailableRestaurants(): List<RestaurantEntity>

    @Query("SELECT * FROM RestaurantEntity WHERE payEazy = 1 AND CAST(rating AS int) > 4")
    suspend fun getPayEazyAndRatedRestaurants(): List<RestaurantEntity>

    @Query("SELECT * FROM RestaurantEntity WHERE payEazy = 1 AND CAST(rating AS int) > 4 AND type = 'veg'")
    suspend fun getPayEazyVegRatedRestaurants(): List<RestaurantEntity>

    @Query("SELECT * FROM RestaurantEntity WHERE payEazy = 1 AND type = 'veg'")
    suspend fun getPayEazyVegRestaurants(): List<RestaurantEntity>

    @Query("SELECT * FROM RestaurantEntity WHERE CAST(rating AS INT) > 4")
    suspend fun getRestaurantsOfHigherRating(): List<RestaurantEntity>

    @Query("SELECT * FROM RestaurantEntity WHERE type = 'veg' AND CAST(rating AS int) > 4")
    suspend fun getVegRatedRestaurants(): List<RestaurantEntity>

    @Query("SELECT * FROM RestaurantEntity WHERE type = 'veg'")
    suspend fun getVegRestaurants(): List<RestaurantEntity>

}


@Dao
interface BarDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNewBar(barEntity: BarEntity)

    @Query("SELECT * FROM BarEntity LIMIT 10")
    suspend fun getAll(): List<BarEntity>

}
