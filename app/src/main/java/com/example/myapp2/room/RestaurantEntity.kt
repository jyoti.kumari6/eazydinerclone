package com.example.myapp2.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class RestaurantEntity(
    @PrimaryKey
    val restaurantName: String,
    val location: String,
    val type: String?,
    val cft: String,
    val rating: String?,
    val payEazy: Int?
)

@Entity
data class BarEntity(
    @PrimaryKey
    val barName: String,
    val location: String,
    val cft: Int?,
    val rating: Int?,
    val payEazy: Int?
)
