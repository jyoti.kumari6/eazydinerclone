package com.example.myapp2.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [RestaurantEntity::class], version = 1, exportSchema = true)
abstract class RestaurantDatabase : RoomDatabase(){

    abstract fun restaurantDao(): RestaurantDao

    companion object {
        @Volatile
        private var INSTANCE: RestaurantDatabase? = null

        fun getRestaurantDataBase(context: Context): RestaurantDatabase {
            synchronized(this) {
                if (INSTANCE === null) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        RestaurantDatabase::class.java,
                        "restaurantDatabase"
                    ).build()
                }
            }
            return INSTANCE!!
        }
    }

}

