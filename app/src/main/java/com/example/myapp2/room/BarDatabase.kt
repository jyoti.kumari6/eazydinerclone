package com.example.myapp2.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [BarEntity::class], version = 1, exportSchema = true)
abstract class BarDatabase : RoomDatabase(){

    abstract fun barDao(): BarDao

    companion object {
        @Volatile
        private var INSTANCE: BarDatabase? = null

        fun getBarDatabase(context: Context): BarDatabase {
            synchronized(this) {
                if (INSTANCE === null) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        BarDatabase::class.java,
                        "barDatabase"
                    ).build()
                }
            }
            return INSTANCE!!
        }
    }
}

