package com.example.myapp2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapp2.R
import com.example.myapp2.room.RestaurantEntity

class RecommendedRestaurantAdapter(val context: Context) : RecyclerView.Adapter<RecommendedRestaurantAdapter.ViewModel>() {
    private var restaurants: List<RestaurantEntity> = listOf()

    fun setData(restaurants: List<RestaurantEntity>) {
        this.restaurants = restaurants
        notifyDataSetChanged()
    }

    inner class ViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameView: TextView = itemView.findViewById(R.id.restaurantName)
        val locationView: TextView = itemView.findViewById(R.id.location)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewModel {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_row_2, parent, false)
        return ViewModel(view)
    }

    override fun getItemCount(): Int {
        return if (restaurants.size < 6) restaurants.size else 6
    }

    override fun onBindViewHolder(holder: ViewModel, position: Int) {
        holder.nameView.text = restaurants[position].restaurantName
        holder.locationView.text = restaurants[position].location
    }
}
