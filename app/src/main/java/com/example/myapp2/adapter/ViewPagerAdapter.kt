package com.example.myapp2.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.myapp2.fragments.LandingPageFirstFragment
import com.example.myapp2.fragments.LandingPageSecondFragment

class ViewPagerAdapter(fragmentManager: FragmentManager): FragmentStatePagerAdapter(fragmentManager){
    private var heading: List<String> = listOf()

    fun setData(heading: List<String>){
        this.heading = heading
    }

    override fun getCount(): Int {

        return heading.size
    }

    override fun getItem(position: Int): Fragment {
      return if(position == 0){
           LandingPageFirstFragment()
       }
       else{
           LandingPageSecondFragment()
       }
    }

    override fun getPageTitle(position: Int): CharSequence {
        return heading[position]
    }
}

