package com.example.myapp2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapp2.ListItem1
import com.example.myapp2.ListItem3
import com.example.myapp2.R
import com.example.myapp2.fragments.OnCardClickListener
import com.example.myapp2.room.RestaurantEntity

class LandingPageAdapter(
    val context: Context,
    private val pickData: List<ListItem1>,
    private val dineData: List<ListItem3>,
    private val restaurantData: List<RestaurantEntity>,
    private val upperHeading: List<String>,
    private val onCardClickListener: OnCardClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_TYPE1 = 1
        const val VIEW_TYPE2 = 2
    }

    inner class ViewHolderType1(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val addBanner: TextView = itemView.findViewById(R.id.bannerHeading)
    }

    inner class ViewHolderType2(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val horizontalRecycleView: RecyclerView = itemView.findViewById(R.id.recyclerView2)
        val heading: TextView = itemView.findViewById(R.id.heading)
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> VIEW_TYPE1
            else -> VIEW_TYPE2
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE1 -> {
                val view = LayoutInflater.from(context).inflate(R.layout.add_banner, parent, false)
                ViewHolderType1(view)
            }
            VIEW_TYPE2 -> {
                val view = LayoutInflater.from(context).inflate(R.layout.landing_page_list_item, parent, false)
                ViewHolderType2(view)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun getItemCount(): Int {
        return upperHeading.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolderType1 -> {
                holder.addBanner.text = upperHeading.getOrNull(position) ?: "Default Heading"
            }
            is ViewHolderType2 -> {
                holder.horizontalRecycleView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                holder.heading.text = upperHeading.getOrNull(position) ?: "Default Heading"
                when (position) {
                    1 -> holder.horizontalRecycleView.adapter = PickUpAdapter(context, pickData, onCardClickListener)
                    2 -> holder.horizontalRecycleView.adapter = RecommendedRestaurantAdapter(context).apply { setData(restaurantData) }
                    3, 4 -> holder.horizontalRecycleView.adapter = DineAnytimeAdapter(context, dineData)
                }
            }
        }
    }
}
