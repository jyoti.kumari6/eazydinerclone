package com.example.myapp2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapp2.ListItem3
import com.example.myapp2.R

class DineAnytimeAdapter(val context: Context, val dineData: List<ListItem3>) :
    RecyclerView.Adapter<DineAnytimeAdapter.ViewModel>() {

    inner class ViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val contain: TextView = itemView.findViewById(R.id.dineType)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DineAnytimeAdapter.ViewModel {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_row_3, parent, false)
        return ViewModel(view)
    }

    override fun getItemCount(): Int {
        return dineData.size
    }

    override fun onBindViewHolder(holder: DineAnytimeAdapter.ViewModel, position: Int) {
        holder.contain.text = dineData[position].dine
    }


}