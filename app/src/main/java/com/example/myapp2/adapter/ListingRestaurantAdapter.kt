package com.example.myapp2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapp2.R
import com.example.myapp2.room.BarEntity
import com.example.myapp2.room.RestaurantEntity


class ListingRestaurantAdapter(val context: Context) : RecyclerView.Adapter<ListingRestaurantAdapter.MyViewModel>() {
    private var restaurants: List<RestaurantEntity> = listOf()
    private var bars: List<BarEntity> = listOf()

    fun setData(restaurants: List<RestaurantEntity>, bars: List<BarEntity>) {
        this.restaurants = restaurants
        this.bars = bars
    }

    inner class MyViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameView: TextView = itemView.findViewById(R.id.title)
        val locationView: TextView = itemView.findViewById(R.id.location)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewModel {
        val view = LayoutInflater.from(context).inflate(R.layout.text_row_item, parent, false)
        return MyViewModel(view)
    }

    override fun getItemCount(): Int {
        return restaurants.size + bars.size
    }

    override fun onBindViewHolder(holder: MyViewModel, position: Int) {
        if (position < restaurants.size) {
            val restaurant = restaurants[position]
            holder.nameView.text = restaurant.restaurantName
            holder.locationView.text = restaurant.location
        } else {
            val bar = bars[position - restaurants.size]
            holder.nameView.text = bar.barName
            holder.locationView.text = bar.location
        }
    }

}
