package com.example.myapp2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapp2.ListItem1
import com.example.myapp2.R
import com.example.myapp2.fragments.OnCardClickListener

class PickUpAdapter(val context: Context, private val pickUpData: List<ListItem1>, private val onCardClickListener: OnCardClickListener) :
    RecyclerView.Adapter<PickUpAdapter.ViewModel>() {

    inner class ViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val contain: TextView = itemView.findViewById(R.id.context)
        val offerDetails: TextView = itemView.findViewById(R.id.offersDetails)
        val cardView: CardView = itemView.findViewById(R.id.cardView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewModel {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_row_1, parent, false)
        return ViewModel(view)
    }

    override fun getItemCount(): Int {
        return pickUpData.size
    }

    override fun onBindViewHolder(holder: ViewModel, position: Int) {
        if(position == 0) {
            holder.cardView.setOnClickListener {
                onCardClickListener.onCardClick()
            }
        }
        holder.contain.text = pickUpData[position].heading
        holder.offerDetails.text = pickUpData[position].offerDetails
    }
}