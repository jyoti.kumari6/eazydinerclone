package com.example.myapp2.activity

import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapp2.R
import com.example.myapp2.adapter.ListingRestaurantAdapter
import com.example.myapp2.databinding.ActivityMainBinding
import com.example.myapp2.fragments.ItemDecoration
import com.example.myapp2.room.BarDatabase
import com.example.myapp2.room.BarEntity
import com.example.myapp2.room.RestaurantDatabase
import com.example.myapp2.room.RestaurantEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ListingRestaurantActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var restaurantData: RestaurantDatabase
    private lateinit var barData: BarDatabase
    private lateinit var restaurants: List<RestaurantEntity>
    private lateinit var bars: List<BarEntity>
    private lateinit var adapter: ListingRestaurantAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        restaurantData = RestaurantDatabase.getRestaurantDataBase(context = this)
        barData = BarDatabase.getBarDatabase(context = this)

        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = ListingRestaurantAdapter(this)
        recyclerView.adapter = adapter

        val spacing = resources.getDimension(R.dimen.space).toInt()
        val itemDecoration = ItemDecoration(this, spacing)
        recyclerView.addItemDecoration(itemDecoration)

        updateData()

        binding.payEazyButton.setOnClickListener {
            binding.payEazyButton.isSelected = !binding.payEazyButton.isSelected
            updateFilter()
        }

        binding.RatingButton.setOnClickListener {
            binding.RatingButton.isSelected = !binding.RatingButton.isSelected
            updateFilter()
        }

        binding.vegButton.setOnClickListener {
            binding.vegButton.isSelected = !binding.vegButton.isSelected
            updateFilter()
        }
    }

    private fun updateData() {
        CoroutineScope(Dispatchers.IO).launch {
            restaurants = restaurantData.restaurantDao().getAll()
            bars = barData.barDao().getAll()

            withContext(Dispatchers.Main) {
                adapter.setData(restaurants, bars)
                adapter.notifyDataSetChanged()
            }
        }
    }

    private fun updateFilter() {
        CoroutineScope(Dispatchers.IO).launch {
            val payEazy = binding.payEazyButton.isSelected
            val rate = binding.RatingButton.isSelected
            val veg = binding.vegButton.isSelected

            val filteredRestaurants: List<RestaurantEntity> = when {
                payEazy && rate && veg -> restaurantData.restaurantDao().getPayEazyVegRatedRestaurants()
                payEazy && rate && !veg -> restaurantData.restaurantDao().getPayEazyAndRatedRestaurants()
                payEazy && !rate && veg -> restaurantData.restaurantDao().getPayEazyVegRestaurants()
                payEazy && !rate && !veg -> restaurantData.restaurantDao().getPayEazyAvailableRestaurants()
                !payEazy && rate && veg -> restaurantData.restaurantDao().getVegRatedRestaurants()
                !payEazy && rate && !veg -> restaurantData.restaurantDao().getRestaurantsOfHigherRating()
                !payEazy && !rate && veg -> restaurantData.restaurantDao().getVegRestaurants()
                else -> restaurantData.restaurantDao().getAll()
            }

            withContext(Dispatchers.Main) {
                adapter.setData(filteredRestaurants, bars)
                adapter.notifyDataSetChanged()
            }
        }
    }
}
