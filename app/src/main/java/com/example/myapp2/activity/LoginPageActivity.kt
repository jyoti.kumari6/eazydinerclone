package com.example.myapp2.activity

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import com.example.myapp2.R
import com.example.myapp2.databinding.ActivityLoginPageBinding
import com.example.myapp2.room.RestaurantDatabase

class LoginPageActivity : ComponentActivity() {
    private lateinit var binding: ActivityLoginPageBinding
    private lateinit var restaurantDatabase: RestaurantDatabase


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginPageBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        binding.getOptButton.isEnabled = true

        binding.editTextPhone.addTextChangedListener {
            val phoneNumber = it.toString()
            if (phoneNumber.length == 1) {
                binding.getOptButton.isEnabled = true
                binding.getOptButton.background =
                    ContextCompat.getDrawable(this, R.drawable.otp_button_enabled)
            } else {
                binding.getOptButton.isEnabled = false
                binding.getOptButton.background =
                    ContextCompat.getDrawable(this, R.drawable.otp_button_enabled)
            }
        }

        binding.getOptButton.setOnClickListener {
            if (binding.getOptButton.isEnabled) {
                    val intent = Intent(this, OptVerificationActivity::class.java)
                    startActivity(intent)
            }
        }
    }
}
