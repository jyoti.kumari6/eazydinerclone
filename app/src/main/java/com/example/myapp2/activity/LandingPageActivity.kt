package com.example.myapp2.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.example.myapp2.adapter.ViewPagerAdapter
import com.example.myapp2.databinding.ActivityLandingPageBinding

class LandingPageActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLandingPageBinding
    private lateinit var adapter: ViewPagerAdapter
    private lateinit var heading: List<String>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLandingPageBinding.inflate(layoutInflater)
        setContentView(binding.root)

        heading = listOf("Book A Table", "Walk-In Offers")

        val pager: ViewPager = binding.pager
        adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.setData(heading)
        pager.adapter = adapter


    }

}
