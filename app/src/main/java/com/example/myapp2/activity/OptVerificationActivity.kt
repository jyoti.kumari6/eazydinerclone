package com.example.myapp2.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import com.example.myapp2.R
import com.example.myapp2.databinding.ActivityOptVerificationBinding

class OptVerificationActivity : AppCompatActivity() {
    private lateinit var binding: ActivityOptVerificationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOptVerificationBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
        binding.editTextNumberSigned2.addTextChangedListener {
            val otp = it.toString()
            if (otp.length == 1) {
                binding.verificationButton.isEnabled = true
                binding.verificationButton.background =
                    ContextCompat.getDrawable(this, R.drawable.otp_button_enabled)
                binding.verificationButton.setOnClickListener {
                    val intent = Intent(this, ListingRestaurantActivity::class.java)
                    startActivity(intent)
                }

            } else {
                binding.verificationButton.isEnabled = false
                binding.verificationButton.background =
                    ContextCompat.getDrawable(this, R.drawable.otp_button_enabled)
            }
        }
    }
}